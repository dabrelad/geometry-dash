# Geometry Dash

# Controles

- Space para saltar dentro del nivel.
- R para salir del nivel actual.
- A y D en el menú niveles para cambiar entre ellos.


# Segunda Entrega

Requisitos mínimos

# Clases Abstractas

He utilizado los portales para implementar esta caracteristica a mi proyecto. Portales seria la clase padre abstracta donde estará la función efecto que se aplicará a todos los portales.

# Singleton

Mi UI inGame mostrando las muertes del personaje es el singleton de mi videojuego.

# Interficie

Mi interficie está implementada pensando en añadir en un futuro coleccionables adicionales ya que ahora solo dispongo de uno. El nombre de la interficie es coleccionable.

# Cambio de nivel

He implementado dos niveles y la lógica de ambos y para cambiar de uno a otro está implementada entre la clase mapa y la clase UI.

# Mejoras Adicionales

He incorporado la función spritesCaen dentro de la clase Mapa, que le da un toque de parecido al Geometry Dash original haciendo que los bloques alejados se caigan del mapa.

Aparte he añadido un modo debug que solo se puede acceder a él a traves de código, este modo lo usaba para crear el mapa.

Añadí también una nueva mecánica que es la bola de doble salto, esta se puede ver en el segundo nivel justo al empezar.

Además arreglé ciertos bugs.

# Última entrega
s
Pequeñas mejoras y añadidos.

# Mejoras Adicionales

He incorporado una función para cargar las muertes que llevas en el juego y otra para guardar. 
Aparte he fixeado pequeños bugs.