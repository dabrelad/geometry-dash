package Juego;

import java.util.ArrayList;

import Motor.Field;
import Motor.Sprite;
import Motor.Window;

public class Mapa {

	/**
	 * Lista de sprites que actuan como suelo
	 */
	static ArrayList<Sprite> sprites = new ArrayList<Sprite>();
	/**
	 * Lista de espinas colisionables
	 */
	public static ArrayList<Sprite> espinas = new ArrayList<Sprite>();
	/**
	 * Lista de portales que cambian la gravedad
	 */
	public static ArrayList<Sprite> portalesGravedad = new ArrayList<Sprite>();
	/**
	 * Lista con los portales que te transforman en cohete
	 */
	public static ArrayList<Sprite> portalesCohete = new ArrayList<Sprite>();
	/**
	 * Lista con los portales que te transforman al estado normal. 
	 */
	public static ArrayList<Sprite> portalesCohete2 = new ArrayList<Sprite>();
	/**
	 * Lista con trampolines
	 */
	public static ArrayList<Sprite> trampolinesDobleSalto = new ArrayList<Sprite>();
	/**
	 * Lista con monedas
	 */
	static ArrayList<Sprite> monedas = new ArrayList<Sprite>();
	/**
	 * Lista con fondos
	 */
	static ArrayList<Sprite> backgrounds = new ArrayList<Sprite>();
	
	static ArrayList<Sprite> bolas = new ArrayList<Sprite>();
	
	static ArrayList<Sprite> todos = new ArrayList<Sprite>();

	public static int monedasPrimerNivel = 0;
	public static boolean firstTime = true;
	public static boolean nivelMecanicas = false;
	public static boolean segundoNivel = false;

	/**
	 * Esta función genera el primer nivel al ser llamada
	 * 
	 * @param f field.
	 */
	public static void generarNivelMecanicas() {
		nivelMecanicas = true;
		segundoNivel=false;
		sprites.clear();
		espinas.clear();
		portalesGravedad.clear();
		portalesCohete.clear();
		portalesCohete2.clear();
		trampolinesDobleSalto.clear();
		monedas.clear();
		backgrounds.clear();
		bolas.clear();
		todos.clear();
		if(firstTime) {
			Music.playMusic();
			firstTime = false;
		}
		Main.f.background = null;
		CuadradoSuelo bloquem2 = new CuadradoSuelo(50, 475, 100, 525, "SpriteSuelo.png");
		sprites.add(bloquem2);
		CuadradoSuelo bloquem1 = new CuadradoSuelo(100, 475, 150, 525, "SpriteSuelo.png");
		sprites.add(bloquem1);
		CuadradoSuelo bloque0 = new CuadradoSuelo(150, 475, 200, 525, "SpriteSuelo.png");
		sprites.add(bloque0);
		CuadradoSuelo bloque1 = new CuadradoSuelo(200, 475, 250, 525, "SpriteSuelo.png");
		sprites.add(bloque1);
		CuadradoSuelo bloque2 = new CuadradoSuelo(250, 475, 300, 525, "SpriteSuelo.png");
		sprites.add(bloque2);
		CuadradoSuelo bloque3 = new CuadradoSuelo(300, 475, 350, 525, "SpriteSuelo.png");
		sprites.add(bloque3);
		CuadradoSuelo bloque4 = new CuadradoSuelo(350, 475, 400, 525, "SpriteSuelo.png");
		sprites.add(bloque4);
		CuadradoSuelo bloque5 = new CuadradoSuelo(400, 475, 450, 525, "SpriteSuelo.png");
		sprites.add(bloque5);
		CuadradoSuelo bloque6 = new CuadradoSuelo(450, 475, 500, 525, "SpriteSuelo.png");
		sprites.add(bloque6);
		CuadradoSuelo bloque7 = new CuadradoSuelo(500, 475, 550, 525, "SpriteSuelo.png");
		sprites.add(bloque7);
		CuadradoSuelo bloque8 = new CuadradoSuelo(550, 475, 600, 525, "SpriteSuelo.png");
		sprites.add(bloque8);
		CuadradoSuelo bloque9 = new CuadradoSuelo(600, 475, 650, 525, "SpriteSuelo.png");
		sprites.add(bloque9);
		CuadradoSuelo bloque10 = new CuadradoSuelo(650, 475, 700, 525, "SpriteSuelo.png");
		sprites.add(bloque10);
		CuadradoSuelo bloque11 = new CuadradoSuelo(700, 475, 750, 525, "SpriteSuelo.png");
		sprites.add(bloque11);
		CuadradoSuelo bloque12 = new CuadradoSuelo(750, 475, 800, 525, "SpriteSuelo.png");
		sprites.add(bloque12);
		CuadradoSuelo bloque13 = new CuadradoSuelo(800, 475, 850, 525, "SpriteSuelo.png");
		sprites.add(bloque13);
		CuadradoSuelo bloque14 = new CuadradoSuelo(850, 475, 900, 525, "SpriteSuelo.png");
		sprites.add(bloque14);
		CuadradoSuelo bloque15 = new CuadradoSuelo(900, 475, 950, 525, "SpriteSuelo.png");
		sprites.add(bloque15);
		CuadradoSuelo bloque16 = new CuadradoSuelo(950, 475, 1000, 525, "SpriteSuelo.png");
		sprites.add(bloque16);
		CuadradoSuelo bloque17 = new CuadradoSuelo(1000, 475, 1050, 525, "SpriteSuelo.png");
		sprites.add(bloque17);
		CuadradoSuelo bloque18 = new CuadradoSuelo(1050, 475, 1100, 525, "SpriteSuelo.png");
		sprites.add(bloque18);
		CuadradoSuelo bloque19 = new CuadradoSuelo(1100, 475, 1150, 525, "SpriteSuelo.png");
		sprites.add(bloque19);
		CuadradoSuelo bloque20 = new CuadradoSuelo(1150, 475, 1200, 525, "SpriteSuelo.png");
		sprites.add(bloque20);
		CuadradoSuelo bloque21 = new CuadradoSuelo(1200, 475, 1250, 525, "SpriteSuelo.png");
		sprites.add(bloque21);
		CuadradoSuelo bloque22 = new CuadradoSuelo(1250, 475, 1300, 525, "SpriteSuelo.png");
		sprites.add(bloque22);
		CuadradoSuelo bloque23 = new CuadradoSuelo(1300, 475, 1350, 525, "SpriteSuelo.png");
		sprites.add(bloque23);
		CuadradoSuelo bloque24 = new CuadradoSuelo(1350, 475, 1400, 525, "SpriteSuelo.png");
		sprites.add(bloque24);
		CuadradoSuelo bloque25 = new CuadradoSuelo(1400, 475, 1450, 525, "SpriteSuelo.png");
		sprites.add(bloque25);

		CuadradoSuelo bloque25coma3 = new CuadradoSuelo(1300, 150, 1400, 200, "SpriteSuelo.png");
		sprites.add(bloque25coma3);
		CuadradoSuelo bloque25coma5 = new CuadradoSuelo(1400, 150, 1450, 200, "SpriteSuelo.png");
		sprites.add(bloque25coma5);
		CuadradoSuelo bloque26 = new CuadradoSuelo(1450, 150, 1500, 200, "SpriteSuelo.png");
		sprites.add(bloque26);
		CuadradoSuelo bloque27 = new CuadradoSuelo(1500, 150, 1550, 200, "SpriteSuelo.png");
		sprites.add(bloque27);
		CuadradoSuelo bloque28 = new CuadradoSuelo(1550, 150, 1600, 200, "SpriteSuelo.png");
		sprites.add(bloque28);
		CuadradoSuelo bloque29 = new CuadradoSuelo(1600, 150, 1650, 200, "SpriteSuelo.png");
		sprites.add(bloque29);
		CuadradoSuelo bloque30 = new CuadradoSuelo(1650, 150, 1700, 200, "SpriteSuelo.png");
		sprites.add(bloque30);
		CuadradoSuelo bloque31 = new CuadradoSuelo(1700, 150, 1750, 200, "SpriteSuelo.png");
		sprites.add(bloque31);
		CuadradoSuelo bloque32 = new CuadradoSuelo(1750, 150, 1800, 200, "SpriteSuelo.png");
		sprites.add(bloque32);
		CuadradoSuelo bloque33 = new CuadradoSuelo(1800, 150, 1850, 200, "SpriteSuelo.png");
		sprites.add(bloque33);
		CuadradoSuelo bloque34 = new CuadradoSuelo(1850, 150, 1900, 200, "SpriteSuelo.png");
		sprites.add(bloque34);
		CuadradoSuelo bloque35 = new CuadradoSuelo(1900, 150, 1950, 200, "SpriteSuelo.png");
		sprites.add(bloque35);
		CuadradoSuelo bloque36 = new CuadradoSuelo(1950, 150, 2000, 200, "SpriteSuelo.png");
		sprites.add(bloque36);
		CuadradoSuelo bloque37 = new CuadradoSuelo(2000, 150, 2050, 200, "SpriteSuelo.png");
		sprites.add(bloque37);
		CuadradoSuelo bloque38 = new CuadradoSuelo(2050, 150, 2100, 200, "SpriteSuelo.png");
		sprites.add(bloque38);
		CuadradoSuelo bloque39 = new CuadradoSuelo(2100, 150, 2150, 200, "SpriteSuelo.png");
		sprites.add(bloque39);

		CuadradoSuelo bloque40 = new CuadradoSuelo(2150, 475, 2200, 525, "SpriteSuelo.png");
		sprites.add(bloque40);
		CuadradoSuelo bloque41 = new CuadradoSuelo(2200, 475, 2250, 525, "SpriteSuelo.png");
		sprites.add(bloque41);
		CuadradoSuelo bloque42 = new CuadradoSuelo(2250, 475, 2300, 525, "SpriteSuelo.png");
		sprites.add(bloque42);
		CuadradoSuelo bloque43 = new CuadradoSuelo(2300, 475, 2350, 525, "SpriteSuelo.png");
		sprites.add(bloque43);
		CuadradoSuelo bloque44 = new CuadradoSuelo(2350, 475, 2400, 525, "SpriteSuelo.png");
		sprites.add(bloque44);
		CuadradoSuelo bloque45 = new CuadradoSuelo(2400, 475, 2450, 525, "SpriteSuelo.png");
		sprites.add(bloque45);
		CuadradoSuelo bloque46 = new CuadradoSuelo(2450, 475, 2500, 525, "SpriteSuelo.png");
		sprites.add(bloque46);
		CuadradoSuelo bloque47 = new CuadradoSuelo(2500, 475, 2550, 525, "SpriteSuelo.png");
		sprites.add(bloque47);
		CuadradoSuelo bloque48 = new CuadradoSuelo(2550, 475, 2600, 525, "SpriteSuelo.png");
		sprites.add(bloque48);
		CuadradoSuelo bloque49 = new CuadradoSuelo(3850, 475, 3900, 525, "SpriteSuelo.png");
		sprites.add(bloque49);

		Portal1 portal0 = new Portal1(1300, 375, 1350, 475, "GravityPortalA.png");
		portalesGravedad.add(portal0);

		Portal1 portal1 = new Portal1(2100, 200, 2150, 300, "GravityPortalA.png");
		portalesGravedad.add(portal1);

		Espina espina0 = new Espina(1450, 475, 1500, 525, "Espina.png");
		espinas.add(espina0);
		Espina espina1 = new Espina(1500, 475, 1550, 525, "Espina.png");
		espinas.add(espina1);
		Espina espina2 = new Espina(1550, 475, 1600, 525, "Espina.png");
		espinas.add(espina2);
		Espina espina3 = new Espina(1600, 475, 1650, 525, "Espina.png");
		espinas.add(espina3);
		Espina espina4 = new Espina(1650, 475, 1700, 525, "Espina.png");
		espinas.add(espina4);
		Espina espina5 = new Espina(1700, 475, 1750, 525, "Espina.png");
		espinas.add(espina5);
		Espina espina6 = new Espina(1750, 475, 1800, 525, "Espina.png");
		espinas.add(espina6);
		Espina espina7 = new Espina(1800, 475, 1850, 525, "Espina.png");
		espinas.add(espina7);
		Espina espina8 = new Espina(1850, 475, 1900, 525, "Espina.png");
		espinas.add(espina8);
		Espina espina9 = new Espina(1900, 475, 1950, 525, "Espina.png");
		espinas.add(espina9);
		Espina espina10 = new Espina(1950, 475, 2000, 525, "Espina.png");
		espinas.add(espina10);

		Espina espina11 = new Espina(3000, 150, 3050, 200, "Espina.png");
		espina11.angle = 180;
		espinas.add(espina11);
		Espina espina12 = new Espina(3050, 150, 3100, 200, "Espina.png");
		espina12.angle = 180;
		espinas.add(espina12);
		Espina espina13 = new Espina(3100, 150, 3150, 200, "Espina.png");
		espina13.angle = 180;
		espinas.add(espina13);
		Espina espina14 = new Espina(3150, 150, 3200, 200, "Espina.png");
		espina14.angle = 180;
		espinas.add(espina14);

		if (monedasPrimerNivel == 0) {
			Moneda moneda0 = new Moneda(2550, 425, 2600, 475, "MonedaAzul.gif");
			monedas.add(moneda0);
		}

		PortalCohete portalCohete0 = new PortalCohete(2600, 350, 2650, 475, "ShipPortal.png");
		portalesCohete.add(portalCohete0);

		PortalCoheteANormal PortalCoheteANormal1 = new PortalCoheteANormal(3800, 300, 3850, 425, "ShipPortal.png");
		portalesCohete2.add(PortalCoheteANormal1);

		//TrampolinDobleSalto t1 = new TrampolinDobleSalto(600, 460, 650, 475);
		//trampolinesDobleSalto.add(t1);
		
		BolaDobleSalto b0 = new BolaDobleSalto(800, 375, 850, 425);
		bolas.add(b0);

		// Final primer nivel
		todos.addAll(sprites);
		todos.addAll(espinas);
		todos.addAll(portalesGravedad);
		todos.addAll(monedas);
		todos.addAll(portalesCohete);
		todos.addAll(portalesCohete2);
		todos.addAll(trampolinesDobleSalto);
		todos.addAll(bolas);
		Main.f.add(UIGame.getUI());
		Main.f.add(todos);

	}
	
	public static void generarSegundoNivel() {
		nivelMecanicas = false;
		segundoNivel=true;
		sprites.clear();
		espinas.clear();
		portalesGravedad.clear();
		portalesCohete.clear();
		portalesCohete2.clear();
		trampolinesDobleSalto.clear();
		monedas.clear();
		backgrounds.clear();
		bolas.clear();
		todos.clear();
		if(firstTime) {
			Music.playMusic();
			firstTime = false;
		}
		Main.f.background = null;
		segundoNivel = true; 
		
		CuadradoSuelo bloque0 = new CuadradoSuelo(150, 475, 200, 525, "SpriteSuelo.png");
		sprites.add(bloque0);
		CuadradoSuelo bloque1 = new CuadradoSuelo(200, 475, 250, 525, "SpriteSuelo.png");
		sprites.add(bloque1);
		CuadradoSuelo bloque2 = new CuadradoSuelo(250, 475, 300, 525, "SpriteSuelo.png");
		sprites.add(bloque2);
		CuadradoSuelo bloque3 = new CuadradoSuelo(300, 475, 350, 525, "SpriteSuelo.png");
		sprites.add(bloque3);
		CuadradoSuelo bloque4 = new CuadradoSuelo(350, 475, 400, 525, "SpriteSuelo.png");
		sprites.add(bloque4);
		CuadradoSuelo bloque5 = new CuadradoSuelo(400, 475, 450, 525, "SpriteSuelo.png");
		sprites.add(bloque5);
		CuadradoSuelo bloque6 = new CuadradoSuelo(450, 475, 500, 525, "SpriteSuelo.png");
		sprites.add(bloque6);
		CuadradoSuelo bloque7 = new CuadradoSuelo(500, 475, 550, 525, "SpriteSuelo.png");
		sprites.add(bloque7);
		CuadradoSuelo bloque8 = new CuadradoSuelo(550, 475, 600, 525, "SpriteSuelo.png");
		sprites.add(bloque8);
		CuadradoSuelo bloque9 = new CuadradoSuelo(600, 475, 650, 525, "SpriteSuelo.png");
		sprites.add(bloque9);
		CuadradoSuelo bloque10 = new CuadradoSuelo(650, 475, 700, 525, "SpriteSuelo.png");
		sprites.add(bloque10);
		CuadradoSuelo bloque11 = new CuadradoSuelo(700, 475, 750, 525, "SpriteSuelo.png");
		sprites.add(bloque11);
		CuadradoSuelo bloque12 = new CuadradoSuelo(750, 475, 800, 525, "SpriteSuelo.png");
		sprites.add(bloque12);
		CuadradoSuelo bloque13 = new CuadradoSuelo(800, 475, 850, 525, "SpriteSuelo.png");
		sprites.add(bloque13);
		CuadradoSuelo bloque14 = new CuadradoSuelo(850, 475, 900, 525, "SpriteSuelo.png");
		sprites.add(bloque14);
		CuadradoSuelo bloque15 = new CuadradoSuelo(900, 475, 950, 525, "SpriteSuelo.png");
		sprites.add(bloque15);
		CuadradoSuelo bloque16 = new CuadradoSuelo(950, 475, 1000, 525, "SpriteSuelo.png");
		sprites.add(bloque16);
		CuadradoSuelo bloque17 = new CuadradoSuelo(1000, 475, 1050, 525, "SpriteSuelo.png");
		sprites.add(bloque17);
		CuadradoSuelo bloque18 = new CuadradoSuelo(1050, 475, 1100, 525, "SpriteSuelo.png");
		sprites.add(bloque18);
		CuadradoSuelo bloque19 = new CuadradoSuelo(1100, 475, 1150, 525, "SpriteSuelo.png");
		sprites.add(bloque19);
		CuadradoSuelo bloque20 = new CuadradoSuelo(1150, 475, 1200, 525, "SpriteSuelo.png");
		sprites.add(bloque20);
		CuadradoSuelo bloque21 = new CuadradoSuelo(1200, 475, 1250, 525, "SpriteSuelo.png");
		sprites.add(bloque21);
		Espina espina0 = new Espina(1200, 425, 1250, 475, "Espina.png");
		espinas.add(espina0);
		CuadradoSuelo bloque22 = new CuadradoSuelo(1250, 475, 1300, 525, "SpriteSuelo.png");
		sprites.add(bloque22);
		CuadradoSuelo bloque23 = new CuadradoSuelo(1300, 475, 1350, 525, "SpriteSuelo.png");
		sprites.add(bloque23);
		CuadradoSuelo bloque24 = new CuadradoSuelo(1350, 475, 1400, 525, "SpriteSuelo.png");
		sprites.add(bloque24);
		CuadradoSuelo bloque25 = new CuadradoSuelo(1400, 475, 1450, 525, "SpriteSuelo.png");
		sprites.add(bloque25);
		CuadradoSuelo bloque26 = new CuadradoSuelo(1450, 475, 1500, 525, "SpriteSuelo.png");
		sprites.add(bloque26);
		CuadradoSuelo bloque27 = new CuadradoSuelo(1500, 475, 1550, 525, "SpriteSuelo.png");
		sprites.add(bloque27);
		CuadradoSuelo bloque28 = new CuadradoSuelo(1550, 475, 1600, 525, "SpriteSuelo.png");
		sprites.add(bloque28);
		CuadradoSuelo bloque29 = new CuadradoSuelo(1600, 475, 1650, 525, "SpriteSuelo.png");
		sprites.add(bloque29);
		CuadradoSuelo bloque30 = new CuadradoSuelo(1650, 475, 1700, 525, "SpriteSuelo.png");
		sprites.add(bloque30);
		CuadradoSuelo bloque31 = new CuadradoSuelo(1700, 475, 1750, 525, "SpriteSuelo.png");
		sprites.add(bloque31);
		Espina espina1 = new Espina(1750, 450, 1800, 475, "Espina.png");
		espinas.add(espina1);
		Espina espina2 = new Espina(1800, 425, 1850, 475, "Espina.png");
		espinas.add(espina2);
		CuadradoSuelo bloque32 = new CuadradoSuelo(1750, 475, 1800, 525, "SpriteSuelo.png");
		sprites.add(bloque32);
		CuadradoSuelo bloque33 = new CuadradoSuelo(1800, 475, 1850, 525, "SpriteSuelo.png");
		sprites.add(bloque33);
		CuadradoSuelo bloque34 = new CuadradoSuelo(1850, 475, 1900, 525, "SpriteSuelo.png");
		sprites.add(bloque34);
		CuadradoSuelo bloque35 = new CuadradoSuelo(1900, 475, 1950, 525, "SpriteSuelo.png");
		sprites.add(bloque35);
		CuadradoSuelo bloque36 = new CuadradoSuelo(1950, 475, 2000, 525, "SpriteSuelo.png");
		sprites.add(bloque36);
		CuadradoSuelo bloque37 = new CuadradoSuelo(2000, 475, 2050, 525, "SpriteSuelo.png");
		sprites.add(bloque37);
		CuadradoSuelo bloque38 = new CuadradoSuelo(2050, 475, 2100, 525, "SpriteSuelo.png");
		sprites.add(bloque38);
		CuadradoSuelo bloque39 = new CuadradoSuelo(2100, 475, 2150, 525, "SpriteSuelo.png");
		sprites.add(bloque39);
		CuadradoSuelo bloque40 = new CuadradoSuelo(2150, 475, 2200, 525, "SpriteSuelo.png");
		sprites.add(bloque40);
		CuadradoSuelo bloque41 = new CuadradoSuelo(2200, 475, 2250, 525, "SpriteSuelo.png");
		sprites.add(bloque41);
		CuadradoSuelo bloque42 = new CuadradoSuelo(2250, 475, 2300, 525, "SpriteSuelo.png");
		sprites.add(bloque42);
		CuadradoSuelo bloque43 = new CuadradoSuelo(2300, 475, 2350, 525, "SpriteSuelo.png");
		sprites.add(bloque43);
		CuadradoSuelo bloque44 = new CuadradoSuelo(2350, 475, 2400, 525, "SpriteSuelo.png");
		sprites.add(bloque44);
		CuadradoSuelo bloque45 = new CuadradoSuelo(2400, 475, 2450, 525, "SpriteSuelo.png");
		sprites.add(bloque45);
		CuadradoSuelo bloque46 = new CuadradoSuelo(2450, 475, 2500, 525, "SpriteSuelo.png");
		sprites.add(bloque46);
		CuadradoSuelo bloque47 = new CuadradoSuelo(2500, 475, 2550, 525, "SpriteSuelo.png");
		sprites.add(bloque47);
		Espina espina3 = new Espina(2500, 425, 2550, 475, "Espina.png");
		espinas.add(espina3);
		Espina espina4 = new Espina(2550, 425, 2600, 475, "Espina.png");
		espinas.add(espina4);
		CuadradoSuelo bloque48 = new CuadradoSuelo(2550, 475, 2600, 525, "SpriteSuelo.png");
		sprites.add(bloque48);
		CuadradoSuelo bloque49 = new CuadradoSuelo(2600, 425, 2650, 475, "SpriteSuelo.png");
		sprites.add(bloque49);
		CuadradoSuelo bloque50 = new CuadradoSuelo(2650, 475, 2700, 525, "SpriteSuelo.png");
		sprites.add(bloque50);
		CuadradoSuelo bloque51 = new CuadradoSuelo(2700, 475, 2750, 525, "SpriteSuelo.png");
		sprites.add(bloque51);
		CuadradoSuelo bloque52 = new CuadradoSuelo(2750, 475, 2800, 525, "SpriteSuelo.png");
		sprites.add(bloque52);
		CuadradoSuelo bloque53 = new CuadradoSuelo(2800, 475, 2850, 525, "SpriteSuelo.png");
		sprites.add(bloque53);
		CuadradoSuelo bloque54 = new CuadradoSuelo(2600, 475, 2650, 525, "SpriteSuelo.png");
		sprites.add(bloque54);
		CuadradoSuelo bloque55 = new CuadradoSuelo(2650, 475, 2700, 525, "SpriteSuelo.png");
		sprites.add(bloque55);
		CuadradoSuelo bloque56 = new CuadradoSuelo(2700, 475, 2750, 525, "SpriteSuelo.png");
		sprites.add(bloque56);
		CuadradoSuelo bloque57 = new CuadradoSuelo(2800, 425, 2850, 475, "SpriteSuelo.png");
		sprites.add(bloque57);
		CuadradoSuelo bloque58 = new CuadradoSuelo(2800, 375, 2850, 425, "SpriteSuelo.png");
		sprites.add(bloque58);
		CuadradoSuelo bloque59 = new CuadradoSuelo(2800, 475, 2850, 525, "SpriteSuelo.png");
		sprites.add(bloque59);
		CuadradoSuelo bloque60 = new CuadradoSuelo(2800, 475, 2850, 525, "SpriteSuelo.png");
		sprites.add(bloque60);
		CuadradoSuelo bloque61 = new CuadradoSuelo(2800, 475, 2850, 525, "SpriteSuelo.png");
		sprites.add(bloque61);
		CuadradoSuelo bloque62 = new CuadradoSuelo(2800, 475, 2850, 525, "SpriteSuelo.png");
		sprites.add(bloque62);
		CuadradoSuelo bloque63 = new CuadradoSuelo(2850, 475, 2900, 525, "SpriteSuelo.png");
		sprites.add(bloque63);
		CuadradoSuelo bloque64 = new CuadradoSuelo(2900, 475, 2950, 525, "SpriteSuelo.png");
		sprites.add(bloque64);
		CuadradoSuelo bloque65 = new CuadradoSuelo(2950, 475, 3000, 525, "SpriteSuelo.png");
		sprites.add(bloque65);
		CuadradoSuelo bloque66 = new CuadradoSuelo(3050, 475, 3100, 525, "SpriteSuelo.png");
		sprites.add(bloque66);
		CuadradoSuelo bloque67 = new CuadradoSuelo(3050, 425, 3100, 475, "SpriteSuelo.png");
		sprites.add(bloque67);
		CuadradoSuelo bloque68 = new CuadradoSuelo(3050, 375, 3100, 425, "SpriteSuelo.png");
		sprites.add(bloque68);
		CuadradoSuelo bloque69 = new CuadradoSuelo(3050, 325, 3100, 375, "SpriteSuelo.png");
		sprites.add(bloque69);
		CuadradoSuelo bloque70 = new CuadradoSuelo(3000, 475, 3050, 525, "SpriteSuelo.png");
		sprites.add(bloque70);
		Espina espina6 = new Espina(2700, 450, 2750, 475, "Espina.png");
		espinas.add(espina6);
		Espina espina7 = new Espina(2750, 450, 2800, 475, "Espina.png");
		espinas.add(espina7);
		Espina espina9 = new Espina(2650, 450, 2700, 475, "Espina.png");
		espinas.add(espina9);
		Espina espina10 = new Espina(2850, 450, 2900, 475, "Espina.png");
		espinas.add(espina10);
		Espina espina11 = new Espina(2900, 450, 2950, 475, "Espina.png");
		espinas.add(espina11);
		Espina espina12 = new Espina(2950, 450, 3000, 475, "Espina.png");
		espinas.add(espina12);
		Espina espina13 = new Espina(3000, 450, 3050, 475, "Espina.png");
		espinas.add(espina13);
		CuadradoSuelo bloque71 = new CuadradoSuelo(3050, 475, 3100, 525, "SpriteSuelo.png");
		sprites.add(bloque71);
		CuadradoSuelo bloque72 = new CuadradoSuelo(3100, 475, 3150, 525, "SpriteSuelo.png");
		sprites.add(bloque72);
		CuadradoSuelo bloque73 = new CuadradoSuelo(3150, 475, 3200, 525, "SpriteSuelo.png");
		sprites.add(bloque73);
		CuadradoSuelo bloque74 = new CuadradoSuelo(3200, 475, 3250, 525, "SpriteSuelo.png");
		sprites.add(bloque74);
		CuadradoSuelo bloque75 = new CuadradoSuelo(3250, 475, 3300, 525, "SpriteSuelo.png");
		sprites.add(bloque75);
		CuadradoSuelo bloque76 = new CuadradoSuelo(3300, 475, 3350, 525, "SpriteSuelo.png");
		sprites.add(bloque76);
		CuadradoSuelo bloque77 = new CuadradoSuelo(3350, 475, 3400, 525, "SpriteSuelo.png");
		sprites.add(bloque77);
		CuadradoSuelo bloque78 = new CuadradoSuelo(3400, 475, 3450, 525, "SpriteSuelo.png");
		sprites.add(bloque78);
		CuadradoSuelo bloque79 = new CuadradoSuelo(3450, 475, 3500, 525, "SpriteSuelo.png");
		sprites.add(bloque79);
		CuadradoSuelo bloque80 = new CuadradoSuelo(3500, 475, 3550, 525, "SpriteSuelo.png");
		sprites.add(bloque80);
		CuadradoSuelo bloque81 = new CuadradoSuelo(3550, 475, 3600, 525, "SpriteSuelo.png");
		sprites.add(bloque81);
		CuadradoSuelo bloque82 = new CuadradoSuelo(3600, 475, 3650, 525, "SpriteSuelo.png");
		sprites.add(bloque82);
		CuadradoSuelo bloque83 = new CuadradoSuelo(3650, 475, 3700, 525, "SpriteSuelo.png");
		sprites.add(bloque83);
		CuadradoSuelo bloque84 = new CuadradoSuelo(3700, 475, 3750, 525, "SpriteSuelo.png");
		sprites.add(bloque84);
		CuadradoSuelo bloque85 = new CuadradoSuelo(3750, 475, 3800, 525, "SpriteSuelo.png");
		sprites.add(bloque85);
		CuadradoSuelo bloque86 = new CuadradoSuelo(3800, 475, 3850, 525, "SpriteSuelo.png");
		sprites.add(bloque86);
		CuadradoSuelo bloque87 = new CuadradoSuelo(3850, 475, 3900, 525, "SpriteSuelo.png");
		sprites.add(bloque87);
		Espina espina14 = new Espina(3800, 425, 3850, 475, "Espina.png");
		espinas.add(espina14);
		Espina espina15 = new Espina(3850, 425, 3900, 475, "Espina.png");
		espinas.add(espina15);
		CuadradoSuelo bloque88 = new CuadradoSuelo(3900, 475, 3950, 525, "SpriteSuelo.png");
		sprites.add(bloque88);
		CuadradoSuelo bloque89 = new CuadradoSuelo(3950, 475, 4000, 525, "SpriteSuelo.png");
		sprites.add(bloque89);
		CuadradoSuelo bloque90 = new CuadradoSuelo(4000, 475, 4050, 525, "SpriteSuelo.png");
		sprites.add(bloque90);
		CuadradoSuelo bloque91 = new CuadradoSuelo(4050, 475, 4100, 525, "SpriteSuelo.png");
		sprites.add(bloque91);
		CuadradoSuelo bloque92 = new CuadradoSuelo(4100, 475, 4150, 525, "SpriteSuelo.png");
		sprites.add(bloque92);
		CuadradoSuelo bloque93 = new CuadradoSuelo(4150, 475, 4200, 525, "SpriteSuelo.png");
		sprites.add(bloque93);
		CuadradoSuelo bloque94 = new CuadradoSuelo(4200, 475, 4250, 525, "SpriteSuelo.png");
		sprites.add(bloque94);
		CuadradoSuelo bloque95 = new CuadradoSuelo(4250, 475, 4300, 525, "SpriteSuelo.png");
		sprites.add(bloque95);
		CuadradoSuelo bloque96 = new CuadradoSuelo(4300, 475, 4350, 525, "SpriteSuelo.png");
		sprites.add(bloque96);
		CuadradoSuelo bloque97 = new CuadradoSuelo(4350, 475, 4400, 525, "SpriteSuelo.png");
		sprites.add(bloque97);
		CuadradoSuelo bloque98 = new CuadradoSuelo(4400, 475, 4450, 525, "SpriteSuelo.png");
		sprites.add(bloque98);
		CuadradoSuelo bloque99 = new CuadradoSuelo(4450, 475, 4500, 525, "SpriteSuelo.png");
		sprites.add(bloque99);
		CuadradoSuelo bloque100 = new CuadradoSuelo(4500, 475, 4550, 525, "SpriteSuelo.png");
		sprites.add(bloque100);
		CuadradoSuelo bloque101 = new CuadradoSuelo(4150, 425, 4200, 475, "SpriteSuelo.png");
		sprites.add(bloque101);
		CuadradoSuelo bloque102 = new CuadradoSuelo(4200, 425, 4250, 475, "SpriteSuelo.png");
		sprites.add(bloque102);
		CuadradoSuelo bloque103 = new CuadradoSuelo(4250, 425, 4300, 475, "SpriteSuelo.png");
		sprites.add(bloque103);
		CuadradoSuelo bloque104 = new CuadradoSuelo(4300, 425, 4350, 475, "SpriteSuelo.png");
		sprites.add(bloque104);
		CuadradoSuelo bloque105 = new CuadradoSuelo(4350, 425, 4400, 475, "SpriteSuelo.png");
		sprites.add(bloque105);
		CuadradoSuelo bloque106 = new CuadradoSuelo(4400, 425, 4450, 475, "SpriteSuelo.png");
		sprites.add(bloque106);
		CuadradoSuelo bloque107 = new CuadradoSuelo(4450, 425, 4500, 475, "SpriteSuelo.png");
		sprites.add(bloque107);
		CuadradoSuelo bloque108 = new CuadradoSuelo(4500, 425, 4550, 475, "SpriteSuelo.png");
		sprites.add(bloque108);
		Espina espina16 = new Espina(4550, 425, 4600, 475, "Espina.png");
		espinas.add(espina16);
		Espina espina17 = new Espina(4600, 425, 4650, 475, "Espina.png");
		espinas.add(espina17);
		Espina espina18 = new Espina(4650, 425, 4700, 475, "Espina.png");
		espinas.add(espina18);
		Espina espina19 = new Espina(4700, 425, 4750, 475, "Espina.png");
		espinas.add(espina19);
		CuadradoSuelo bloque016 = new CuadradoSuelo(4550, 475, 4600, 525, "SpriteSuelo.png");
		sprites.add(bloque016);
		CuadradoSuelo bloque017 = new CuadradoSuelo(4600, 475, 4650, 525, "SpriteSuelo.png");
		sprites.add(bloque017);
		CuadradoSuelo bloque018 = new CuadradoSuelo(4650, 475, 4700, 525, "SpriteSuelo.png");
		sprites.add(bloque018);
		CuadradoSuelo bloque019 = new CuadradoSuelo(4700, 475, 4750, 525, "SpriteSuelo.png");
		sprites.add(bloque019);
		CuadradoSuelo bloque109 = new CuadradoSuelo(4750, 425, 4800, 475, "SpriteSuelo.png");
		sprites.add(bloque109);
		CuadradoSuelo bloque110 = new CuadradoSuelo(4800, 425, 4850, 475, "SpriteSuelo.png");
		sprites.add(bloque110);
		CuadradoSuelo bloque111 = new CuadradoSuelo(4850, 425, 4900, 475, "SpriteSuelo.png");
		sprites.add(bloque111);
		CuadradoSuelo bloque112 = new CuadradoSuelo(4900, 425, 4950, 475, "SpriteSuelo.png");
		sprites.add(bloque112);
		CuadradoSuelo bloque113 = new CuadradoSuelo(4750, 475, 4800, 525, "SpriteSuelo.png");
		sprites.add(bloque113);
		CuadradoSuelo bloque114 = new CuadradoSuelo(4800, 475, 4850, 525, "SpriteSuelo.png");
		sprites.add(bloque114);
		CuadradoSuelo bloque115 = new CuadradoSuelo(4850, 475, 4900, 525, "SpriteSuelo.png");
		sprites.add(bloque115);
		CuadradoSuelo bloque116 = new CuadradoSuelo(4900, 475, 4950, 525, "SpriteSuelo.png");
		sprites.add(bloque116);
		Espina espina20 = new Espina(4925, 375, 4975, 425, "Espina.png");
		espinas.add(espina20);
		CuadradoSuelo bloque117 = new CuadradoSuelo(4950, 425, 5000, 475, "SpriteSuelo.png");
		sprites.add(bloque117);
		CuadradoSuelo bloque118 = new CuadradoSuelo(5000, 425, 5050, 475, "SpriteSuelo.png");
		sprites.add(bloque118);
		CuadradoSuelo bloque119 = new CuadradoSuelo(5050, 425, 5100, 475, "SpriteSuelo.png");
		sprites.add(bloque119);
		CuadradoSuelo bloque120 = new CuadradoSuelo(5100, 425, 5150, 475, "SpriteSuelo.png");
		sprites.add(bloque120);
		CuadradoSuelo bloque121 = new CuadradoSuelo(4950, 475, 5000, 525, "SpriteSuelo.png");
		sprites.add(bloque121);
		CuadradoSuelo bloque122 = new CuadradoSuelo(5000, 475, 5050, 525, "SpriteSuelo.png");
		sprites.add(bloque122);
		CuadradoSuelo bloque123 = new CuadradoSuelo(5050, 475, 5100, 525, "SpriteSuelo.png");
		sprites.add(bloque123);
		CuadradoSuelo bloque124 = new CuadradoSuelo(5100, 475, 5150, 525, "SpriteSuelo.png");
		sprites.add(bloque124);
		Espina espina21 = new Espina(5150, 500, 5200, 525, "Espina.png");
		espinas.add(espina21);
		Espina espina22 = new Espina(5200, 500, 5250, 525, "Espina.png");
		espinas.add(espina22);
		Espina espina23 = new Espina(5250, 500, 5300, 525, "Espina.png");
		espinas.add(espina23);
		
		CuadradoSuelo bloque125 = new CuadradoSuelo(5300, 375, 5350, 425, "SpriteSuelo.png");
		sprites.add(bloque125);
		CuadradoSuelo bloque126 = new CuadradoSuelo(5350, 375, 5400, 425, "SpriteSuelo.png");
		sprites.add(bloque126);
		CuadradoSuelo bloque127 = new CuadradoSuelo(5400, 375, 5450, 425, "SpriteSuelo.png");
		sprites.add(bloque127);
		
		CuadradoSuelo bloque128 = new CuadradoSuelo(5300, 425, 5350, 475, "SpriteSuelo.png");
		sprites.add(bloque128);
		CuadradoSuelo bloque129 = new CuadradoSuelo(5350, 425, 5400, 475, "SpriteSuelo.png");
		sprites.add(bloque129);
		CuadradoSuelo bloque130 = new CuadradoSuelo(5400, 425, 5450, 475, "SpriteSuelo.png");
		sprites.add(bloque130);
		
		CuadradoSuelo bloque131 = new CuadradoSuelo(5300, 475, 5350, 525, "SpriteSuelo.png");
		sprites.add(bloque131);
		CuadradoSuelo bloque132 = new CuadradoSuelo(5350, 475, 5400, 525, "SpriteSuelo.png");
		sprites.add(bloque132);
		CuadradoSuelo bloque133 = new CuadradoSuelo(5400, 475, 5450, 525, "SpriteSuelo.png");
		sprites.add(bloque133);
		Espina espina24 = new Espina(5475, 325, 5525, 375, "Espina.png");
		espinas.add(espina24);
		
		CuadradoSuelo bloque134= new CuadradoSuelo(5450, 375, 5500, 425, "SpriteSuelo.png");
		sprites.add(bloque134);
		CuadradoSuelo bloque135 = new CuadradoSuelo(5500, 375, 5550, 425, "SpriteSuelo.png");
		sprites.add(bloque135);
		CuadradoSuelo bloque136 = new CuadradoSuelo(5550, 375, 5600, 425, "SpriteSuelo.png");
		sprites.add(bloque136);
		CuadradoSuelo bloque145 = new CuadradoSuelo(5600, 375, 5650, 425, "SpriteSuelo.png");
		sprites.add(bloque145);
		CuadradoSuelo bloque146 = new CuadradoSuelo(5650, 375, 5700, 425, "SpriteSuelo.png");
		sprites.add(bloque146);
		
		CuadradoSuelo bloque137= new CuadradoSuelo(5450, 425, 5500, 475, "SpriteSuelo.png");
		sprites.add(bloque137);
		CuadradoSuelo bloque138 = new CuadradoSuelo(5500, 425, 5550, 475, "SpriteSuelo.png");
		sprites.add(bloque138);
		CuadradoSuelo bloque139 = new CuadradoSuelo(5550, 425, 5600, 475, "SpriteSuelo.png");
		sprites.add(bloque139);
		CuadradoSuelo bloque147 = new CuadradoSuelo(5600, 425, 5650, 475, "SpriteSuelo.png");
		sprites.add(bloque147);
		CuadradoSuelo bloque148 = new CuadradoSuelo(5650, 425, 5700, 475, "SpriteSuelo.png");
		sprites.add(bloque148);
		
		CuadradoSuelo bloque140= new CuadradoSuelo(5450, 475, 5500, 525, "SpriteSuelo.png");
		sprites.add(bloque140);
		CuadradoSuelo bloque141 = new CuadradoSuelo(5500, 475, 5550, 525, "SpriteSuelo.png");
		sprites.add(bloque141);
		CuadradoSuelo bloque142 = new CuadradoSuelo(5550, 475, 5600, 525, "SpriteSuelo.png");
		sprites.add(bloque142);
		CuadradoSuelo bloque143 = new CuadradoSuelo(5600, 475, 5650, 525, "SpriteSuelo.png");
		sprites.add(bloque143);
		CuadradoSuelo bloque144 = new CuadradoSuelo(5650, 475, 5700, 525, "SpriteSuelo.png");
		sprites.add(bloque144);
		
		Espina espina25 = new Espina(5700, 375, 5750, 425, "Espina.png");
		espinas.add(espina25);
		Espina espina26 = new Espina(5750, 375, 5800, 425, "Espina.png");
		espinas.add(espina26);
		Espina espina27 = new Espina(5800, 375, 5850, 425, "Espina.png");
		espinas.add(espina27);
		Espina espina28 = new Espina(5850, 375, 5900, 425, "Espina.png");
		espinas.add(espina28);
		CuadradoSuelo bloque149 = new CuadradoSuelo(5700, 425, 5750, 475, "SpriteSuelo.png");
		sprites.add(bloque149);
		CuadradoSuelo bloque150 = new CuadradoSuelo(5750, 425, 5800, 475, "SpriteSuelo.png");
		sprites.add(bloque150);
		CuadradoSuelo bloque151 = new CuadradoSuelo(5800, 425, 5850, 475, "SpriteSuelo.png");
		sprites.add(bloque151);
		CuadradoSuelo bloque155 = new CuadradoSuelo(5850, 425, 5900, 475, "SpriteSuelo.png");
		sprites.add(bloque155);
		CuadradoSuelo bloque152 = new CuadradoSuelo(5700, 475, 5750, 525, "SpriteSuelo.png");
		sprites.add(bloque152);
		CuadradoSuelo bloque153 = new CuadradoSuelo(5750, 475, 5800, 525, "SpriteSuelo.png");
		sprites.add(bloque153);
		CuadradoSuelo bloque154 = new CuadradoSuelo(5800, 475, 5850, 525, "SpriteSuelo.png");
		sprites.add(bloque154);
		CuadradoSuelo bloque156 = new CuadradoSuelo(5850, 475, 5900, 525, "SpriteSuelo.png");
		sprites.add(bloque156);
		
		CuadradoSuelo bloque157 = new CuadradoSuelo(5900, 425, 5950, 475, "SpriteSuelo.png");
		sprites.add(bloque157);
		CuadradoSuelo bloque158 = new CuadradoSuelo(5950, 425, 6000, 475, "SpriteSuelo.png");
		sprites.add(bloque158);
		CuadradoSuelo bloque159 = new CuadradoSuelo(6000, 425, 6050, 475, "SpriteSuelo.png");
		sprites.add(bloque159);
		CuadradoSuelo bloque160 = new CuadradoSuelo(6050, 425, 6100, 475, "SpriteSuelo.png");
		sprites.add(bloque160);
		CuadradoSuelo bloque161 = new CuadradoSuelo(5900, 475, 5950, 525, "SpriteSuelo.png");
		sprites.add(bloque161);
		CuadradoSuelo bloque162 = new CuadradoSuelo(5950, 475, 6000, 525, "SpriteSuelo.png");
		sprites.add(bloque162);
		CuadradoSuelo bloque163 = new CuadradoSuelo(6000, 475, 6050, 525, "SpriteSuelo.png");
		sprites.add(bloque163);
		CuadradoSuelo bloque164 = new CuadradoSuelo(6050, 475, 6100, 525, "SpriteSuelo.png");
		sprites.add(bloque164);
		CuadradoSuelo bloque165 = new CuadradoSuelo(5900, 375, 5950, 425, "SpriteSuelo.png");
		sprites.add(bloque165);
		CuadradoSuelo bloque166 = new CuadradoSuelo(5950, 375, 6000, 425, "SpriteSuelo.png");
		sprites.add(bloque166);
		CuadradoSuelo bloque167 = new CuadradoSuelo(6000, 375, 6050, 425, "SpriteSuelo.png");
		sprites.add(bloque167);
		CuadradoSuelo bloque168 = new CuadradoSuelo(6050, 375, 6100, 425, "SpriteSuelo.png");
		sprites.add(bloque168);
		CuadradoSuelo bloque169 = new CuadradoSuelo(5900, 325, 5950, 375, "SpriteSuelo.png");
		sprites.add(bloque169);
		CuadradoSuelo bloque170 = new CuadradoSuelo(5950, 325, 6000, 375, "SpriteSuelo.png");
		sprites.add(bloque170);
		CuadradoSuelo bloque171 = new CuadradoSuelo(6000, 325, 6050, 375, "SpriteSuelo.png");
		sprites.add(bloque171);
		CuadradoSuelo bloque172 = new CuadradoSuelo(6050, 325, 6100, 375, "SpriteSuelo.png");
		sprites.add(bloque172);
		
		CuadradoSuelo bloque173 = new CuadradoSuelo(6100, 325, 6150, 375, "SpriteSuelo.png");
		sprites.add(bloque173);
		CuadradoSuelo bloque174 = new CuadradoSuelo(6150, 325, 6200, 375, "SpriteSuelo.png");
		sprites.add(bloque174);
		CuadradoSuelo bloque175 = new CuadradoSuelo(6200, 325, 6250, 375, "SpriteSuelo.png");
		sprites.add(bloque175);
		CuadradoSuelo bloque176 = new CuadradoSuelo(6250, 325, 6300, 375, "SpriteSuelo.png");
		sprites.add(bloque176);
		CuadradoSuelo bloque177 = new CuadradoSuelo(6100, 375, 6150, 425, "SpriteSuelo.png");
		sprites.add(bloque177);
		CuadradoSuelo bloque178 = new CuadradoSuelo(6150, 375, 6200, 425, "SpriteSuelo.png");
		sprites.add(bloque178);
		CuadradoSuelo bloque179 = new CuadradoSuelo(6200, 375, 6250, 425, "SpriteSuelo.png");
		sprites.add(bloque179);
		CuadradoSuelo bloque180 = new CuadradoSuelo(6250, 375, 6300, 425, "SpriteSuelo.png");
		sprites.add(bloque180);
		CuadradoSuelo bloque181 = new CuadradoSuelo(6100, 425, 6150, 475, "SpriteSuelo.png");
		sprites.add(bloque181);
		CuadradoSuelo bloque182 = new CuadradoSuelo(6150, 425, 6200, 475, "SpriteSuelo.png");
		sprites.add(bloque182);
		CuadradoSuelo bloque183 = new CuadradoSuelo(6200, 425, 6250, 475, "SpriteSuelo.png");
		sprites.add(bloque183);
		CuadradoSuelo bloque184 = new CuadradoSuelo(6250, 425, 6300, 475, "SpriteSuelo.png");
		sprites.add(bloque184);
		CuadradoSuelo bloque185 = new CuadradoSuelo(6100, 475, 6150, 525, "SpriteSuelo.png");
		sprites.add(bloque185);
		CuadradoSuelo bloque186 = new CuadradoSuelo(6150, 475, 6200, 525, "SpriteSuelo.png");
		sprites.add(bloque186);
		CuadradoSuelo bloque187 = new CuadradoSuelo(6200, 475, 6250, 525, "SpriteSuelo.png");
		sprites.add(bloque187);
		CuadradoSuelo bloque188 = new CuadradoSuelo(6250, 475, 6300, 525, "SpriteSuelo.png");
		sprites.add(bloque188);
		
		CuadradoSuelo bloque189 = new CuadradoSuelo(6300, 475, 6350, 525, "SpriteSuelo.png");
		sprites.add(bloque189);
		CuadradoSuelo bloque190 = new CuadradoSuelo(6350, 475, 6400, 525, "SpriteSuelo.png");
		sprites.add(bloque190);
		CuadradoSuelo bloque191 = new CuadradoSuelo(6400, 475, 6450, 525, "SpriteSuelo.png");
		sprites.add(bloque191);
		CuadradoSuelo bloque192 = new CuadradoSuelo(6500, 475, 6550, 525, "SpriteSuelo.png");
		sprites.add(bloque192);
		
		
		
		
		
		

		
		// Final primer nivel
		todos.addAll(sprites);
		todos.addAll(espinas);
		todos.addAll(portalesGravedad);
		todos.addAll(monedas);
		todos.addAll(portalesCohete);
		todos.addAll(portalesCohete2);
		todos.addAll(trampolinesDobleSalto);
		todos.addAll(bolas);
		Main.f.add(UIGame.getUI());
		Main.f.add(todos);

	}

	/*
	 * Esta funcion comprueba cuando acabas los niveles y te lleva al menu
	 * correspondiente To fix texto de agradecimiento
	 * 
	 * @param mainPJ Objeto del personaje Principal
	 */

	public static void finalNiveles(PJ mainPJ) {
		int finalNivel1x2 = 8000;

		if (nivelMecanicas) {
			if (mainPJ.x2 >= finalNivel1x2) {
				Texto felicidades = new Texto("", 250, 120, 900, 180, "NIVEL COMPLETADO");
				felicidades.textColor = 0xb81004;
				felicidades.font = Main.w.customFont("GdashFont.otf", 46);
				Main.f.add(felicidades);
				UI.nivelActual = 0;
				UI.selectorNiveles = true;
				Particulas.AParticulas.clear();
				Main.f.clear();
				Main.f.resetScroll();
				UI.inGame = false;
				UI.AccederMenuNiveles(mainPJ);

			}

		}

	}

	/**
	 * Hace que el fondo de los niveles sean sprites que se mueven para dar sensacion de velocidad
	 */
	public static void scrollearFondoNiveles() {
		Background bg1 = new Background(0, 0, 720, 720, "backgrounds/Blue_game_bg_03_001-hd.png");
		backgrounds.add(bg1);
		Background bg2 = new Background(720, 0, 1440, 720, "backgrounds/Blue_game_bg_03_001-hd.png");
		backgrounds.add(bg2);
		Background bg3 = new Background(1440, 0, 2160, 720, "backgrounds/Blue_game_bg_03_001-hd.png");
		backgrounds.add(bg3);
		Background bg4 = new Background(2160, 0, 2880, 720, "backgrounds/Blue_game_bg_03_001-hd.png");
		backgrounds.add(bg4);
		Background bg5 = new Background(2880, 0, 3600, 720, "backgrounds/Blue_game_bg_03_001-hd.png");
		backgrounds.add(bg5);
		Background bg6 = new Background(3600, 0, 4320, 720, "backgrounds/Blue_game_bg_03_001-hd.png");
		backgrounds.add(bg6);
		Background bg7 = new Background(4320, 0, 5040, 720, "backgrounds/Blue_game_bg_03_001-hd.png");
		backgrounds.add(bg7);
		Background bg8 = new Background(5040, 0, 5760, 720, "backgrounds/Blue_game_bg_03_001-hd.png");
		backgrounds.add(bg8);
		Background bg9 = new Background(5760, 0, 6480, 720, "backgrounds/Blue_game_bg_03_001-hd.png");
		backgrounds.add(bg9);
		Background bg10 = new Background(6480, 0, 7200, 720, "backgrounds/Blue_game_bg_03_001-hd.png");
		backgrounds.add(bg10);
		Background bg11 = new Background(7200, 0, 7920, 720, "backgrounds/Blue_game_bg_03_001-hd.png");
		backgrounds.add(bg11);
		Background bg12 = new Background(7920, 0, 8640, 720, "backgrounds/Blue_game_bg_03_001-hd.png");
		backgrounds.add(bg12);
		Background bg13 = new Background(8640, 0, 9360, 720, "backgrounds/Blue_game_bg_03_001-hd.png");
		backgrounds.add(bg13);
		for (int i = 0; i < backgrounds.size(); i++) {
			backgrounds.get(i).x1 += Main.velocidad * 0.8;
			backgrounds.get(i).x2 += Main.velocidad * 0.8;
		}
		Main.f.add(backgrounds);
	}

	public static void spritesCaen() {
		for (Sprite s : Mapa.todos) {
			//System.out.println(s.x1);
			if(s.x1 <= Main.mainPJ.x1 - PJ.mainX + 50) {
				if(Main.fuerzaGravedad > 0) {
					s.y1+=10;
					s.y2+=10;
				}
				else if((Main.fuerzaGravedad < 0)){
					s.y1-=10;
					s.y2-=10;
				}
			}
		}
	}

	public static void refrescarNivel() {
		// TODO Auto-generated method stub

		backgrounds.clear();
		todos.clear();
		if(firstTime) {
			Music.playMusic();
			firstTime = false;
		}
		Main.f.background = null;
		
		// Final primer nivel
		todos.addAll(sprites);
		todos.addAll(espinas);
		todos.addAll(portalesGravedad);
		todos.addAll(monedas);
		todos.addAll(portalesCohete);
		todos.addAll(portalesCohete2);
		todos.addAll(trampolinesDobleSalto);
		todos.addAll(bolas);
		Main.f.add(UIGame.getUI());
		Main.f.add(todos);

	}
}
