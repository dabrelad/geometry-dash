package Juego;

import Motor.Sprite;

public class CuadradoSuelo extends Sprite{

	public CuadradoSuelo(int x1, int y1, int x2, int y2, String path) {
		super("tile", x1, y1, x2, y2, path);
		terrain = true;
		solid = true;
		this.DEPTH=9;
	}

}
