package Juego;

import Motor.Sprite;

public abstract class Portales extends Sprite {
	
	public Portales(int x1, int y1, int x2, int y2, String path) {
		super("portales", x1, y1, x2, y2, path);
		terrain = true;
		solid = true;
		DEPTH = 10;
	}
	
	
	/**
	 * Funci�n global para todas las clases de portal, segun el tipo de portal obtiene un comportamiento el PJ o otro. 
	 * @param portal
	 */
	public static void efecto(tipoPortales portal){
		if(portal == portal.GRAVEDAD) {
			Main.fuerzaGravedad = -Main.fuerzaGravedad;
		}
		else if(portal == portal.COHETE) {
			float fuerzaCohete = Main.fuerzaGravedad + Main.fuerzaGravedad;
			Main.mainPJ.changeImage("Ship0.png");
			if (Main.w.getCurrentKeyPressed() == ' ') {
				Main.mainPJ.y1 -= fuerzaCohete;
				Main.mainPJ.y2 -= fuerzaCohete;
			}
			if (PortalCohete.changingRocket) {
				Main.mainPJ.angle = 0;
			}
		}
		else if(portal == portal.COHETEANORMAL) {
			PortalCohete.changingRocket = false;
			Main.mainPJ.changeImage("PJ.png");
			Main.mainPJ.angle = 0;
		}
	}
	

}
