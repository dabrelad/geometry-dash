package Juego;

import java.util.Timer;

import Motor.Field;
import Motor.Sprite;

public class Moneda extends Sprite implements Coleccionable {
	

	public Moneda(int x1, int y1, int x2, int y2, String string) {
		super("moneda", x1, y1, x2, y2, string);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Comprueba constantemente si el personaje principal está colisionando con una moneda. 
	 * @param mainPJ Objeto del personaje Principal
	 * @param f Field
	 */
	
	public static void colisionMoneda(Sprite mainPJ, Field f) {
		for (Sprite s : mainPJ.collidesWithList(Mapa.monedas)) {
			((Moneda) s).coleccionar();
		}
	}
	
	
	@Override
	/**
	 * Funcion que viene de la interfaz coleccionable, suma al contador de monedas y elimina la moneda. 
	 */
	public void coleccionar() {
			if(Mapa.nivelMecanicas == true) {
				Mapa.monedasPrimerNivel++;
				System.out.println("Mis monedas de este nivel = " + Mapa.monedasPrimerNivel);
			}
			this.delete();
	}
}
