package Juego;

import java.io.IOException;
import java.util.ArrayList;

import Motor.Field;
import Motor.Sprite;
import Motor.Window;

public class PJ extends Sprite {

	// Constructor y zona variables
	public PJ(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		solid = true;
		DEPTH = 9;
		this.physicBody = false;
		// TODO Auto-generated constructor stub
	}
	/**
	 * booleano que indica si estas en el suelo
	 */
	public static boolean enElSuelo = false;
	/**
	 * booleano que indica si estas en el techo
	 */
	public static boolean enElTecho = false;
	/**
	 * booleano que indica si estas saltando
	 */
	public static boolean saltando = false;
	public static boolean segundaFase = false;
	/**
	 * Posicion inicial x1 del personaje
	 */
	public static int mainX = 250;
	/**
	 * Posicion inicial y1 del personaje
	 */
	public static int mainY = 250;
	/**
	 * Booleano que te indica cuando el juego esta pausado.
	 */
	private static boolean paused = false;
	public static int inicioSaltoX = 0;
	public static int inicioSaltoY = 0;
	/**
	 * Variable auxiliar de velocidad. 
	 */
	private static int newVelocity = Main.velocidad;
	// Fin zona variabless

	/**
	 * Inicializa el PJ y lo añade a Field.
	 * @param f field
	 * @return el personaje inicializado.
	 */
	public static PJ inicializarPJ() {
		PJ mainPJ = new PJ("PJ", mainX, mainY, mainX + 50, mainY + 50, "PJ.png");
		Main.f.add(mainPJ);
		Main.f.draw(mainPJ);
		return mainPJ;
	}

	/**
	 * Esta función mueve constantemente al PJ y le añade "gravedad"
	 * @param mainPJ Objeto del Personaje Principal.
	 * @param velocidad Velocidad a la que se moverá el PJ.
	 * @param f Field
	 * @param w Window
	 * @throws IOException 
	 */
	public static void moverPJ(Sprite mainPJ, int velocidad,Window w) throws IOException {
		mainPJ.x1 += velocidad;
		mainPJ.x2 += velocidad;
		gravedad(mainPJ);
		pausarElJuego(w);
		if (mainPJ.y1 >= w.sizeY || mainPJ.y2 <= 0) {
			GameOver.gameOver(PJ.mainX, PJ.mainX + 50, PJ.mainY, PJ.mainY + 50, mainPJ);
		}
		Main.f.scroll(-velocidad, 0);
	}
	
	public static void debugPJ(Sprite mainPJ, int velocidad,Window w) throws IOException {
		if (w.getKeyPressed() == 'D' || w.getKeyPressed() == 'd') {
			mainPJ.x1 += velocidad;
			mainPJ.x2 += velocidad;
		}
		else if (w.getKeyPressed() == 'W' || w.getKeyPressed() == 'w') {
			mainPJ.y1 -= velocidad;
			mainPJ.y2 -= velocidad;
		}
		else if (w.getKeyPressed() == 'S' || w.getKeyPressed() == 's') {
			mainPJ.y1 += velocidad;
			mainPJ.y2 += velocidad;
		}
		else if (w.getKeyPressed() == 'A' || w.getKeyPressed() == 'a') {
			mainPJ.x1 -= velocidad;
			mainPJ.x2 -= velocidad;
		}
		if (mainPJ.y1 >= w.sizeY || mainPJ.y2 <= 0) {
			GameOver.gameOver(PJ.mainX, PJ.mainX + 50, PJ.mainY, PJ.mainY + 50, mainPJ);
		}
		Main.f.lockScroll(mainPJ, w);
		
	}
	
	public static void pausarElJuego(Window w) {
		//getKeyUp?
		if (w.getKeyPressed() == 'P' || w.getKeyPressed() == 'p') {
			if(!paused) {
				paused = true;
			}
			else {
				paused = false;
			}
			Main.velocidad = 0;
		}
		if(paused) {
			System.out.println("Juego pausado");
			Main.velocidad = 0;
		}
		else {
			Main.velocidad = newVelocity;
		}
	}
	
	/**
	 * Esta función comprueba si es posible realizar un salto y salta si es posible. 
	 * @param mainPJ Objeto del Personaje Principal.
	 * @param w Window
	 * @param f Field
	 */
	public static void comprobarSalto(Sprite mainPJ, Window w) {
		BolaDobleSalto.dobleSalto();
		// mientras presionas w, el PJ salta hasta un tope
		if (w.getCurrentKeyPressed() == ' ' && !saltando) {
			inicioSaltoX = mainPJ.x1;
			inicioSaltoY = mainPJ.y1;
		}
		if ((w.getCurrentKeyPressed() == ' ' && enElSuelo || saltando) || ((w.getCurrentKeyPressed() == ' ' && enElTecho) || saltando)) { // comprobacion salto
			if (!Portal1.changingGravity) {
				saltar(mainPJ);
			}
		}
	}

	/**
	 * Ejecuta el salto
	 * @param mainPJ Objeto del Personaje Principal.
	 * @param f Field
	 */
	static void saltar(Sprite mainPJ) {
		int limiteSalto = 150;
		float fuerzaSalto = -33f;
		// Primera fase
		saltando = true;
		if (!Portal1.changingGravity) {
			if (Main.fuerzaGravedad > 0) {
				if (mainPJ.y1 > inicioSaltoY - limiteSalto && !segundaFase) {
					mainPJ.y1 += fuerzaSalto;
					mainPJ.y2 += fuerzaSalto;
					mainPJ.angle += 364.5;
				} else if (mainPJ.y1 <= inicioSaltoY - limiteSalto) {
					segundaFase = true;
				}
				if (segundaFase) {
					mainPJ.angle += 364.5;
				}
			}

			else if (Main.fuerzaGravedad < 0) {
				if (mainPJ.y1 < inicioSaltoY + limiteSalto && !segundaFase) {
					mainPJ.y1 -= fuerzaSalto;
					mainPJ.y2 -= fuerzaSalto;
					mainPJ.angle -= 364.290;
				} else if (mainPJ.y1 >= inicioSaltoY - limiteSalto) {
					segundaFase = true;
				}
				if (segundaFase) {
					mainPJ.angle -= 364.290;
				}
			}
		}

	}

	/**
	 * Añade la fuerza de gravedad y comprueba si el PJ esta colisionando con el suelo o el techo. 
	 * @param mainPJ Objeto del Personaje Principal.
	 * @param f Field. 
	 */
	private static void gravedad(Sprite mainPJ) {

		mainPJ.y1 += Main.fuerzaGravedad;
		mainPJ.y2 += Main.fuerzaGravedad;

		// Colision suelo y techo
		if (!mainPJ.collidesWithList(Mapa.sprites).isEmpty()) {
			mainPJ.getGrounded(Main.f);
			enElSuelo = true;
			reajustarRotacionPJ(mainPJ);
			saltando = false;
			segundaFase = false;

			if (Portal1.changingGravity) { // cambiamos los grados del PJ al usar el portal
				reajustarRotacionPJ(mainPJ);

				Portal1.changingGravity = false;
			}

		} else {
			enElSuelo = false;
		}
		if (mainPJ.isOnCeiling(Main.f)) {
			enElTecho = true;
			reajustarRotacionPJ(mainPJ);
			mainPJ.getCeiling(Main.f);
			saltando = false;
			segundaFase = false;

		} else {
			enElTecho = false;
		}
	}
	
	
	/**
	 * Comprueba si mainPJ está en el suelo o el techo y arregla la rotación si es necesario. 
	 * @param mainPJ Objeto del Personaje Principal. 
	 */
	private static void reajustarRotacionPJ(Sprite mainPJ) {
		if (enElSuelo) {
			if ((mainPJ.angle % 360) <= 0 && (mainPJ.angle % 360) >= -45) {
				mainPJ.angle = 0;
			}
			else if ((mainPJ.angle % 360) < -45 && (mainPJ.angle % 360) >= -135) {
				mainPJ.angle = -90;
			}
			else if ((mainPJ.angle % 360) < -135 && (mainPJ.angle % 360) >= -225) {
				mainPJ.angle = -180;
			}
			else if ((mainPJ.angle % 360) < -225 && (mainPJ.angle % 360) >= -315) {
				mainPJ.angle = -270;
			}
			else if ((mainPJ.angle % 360) < -315 && (mainPJ.angle % 360) >= -360) {
				mainPJ.angle = -360;
			}
			else if ((mainPJ.angle % 360) >= 0 && (mainPJ.angle % 360) <= 45) {
				mainPJ.angle = 0;
			}
			else if ((mainPJ.angle % 360) > 45 && (mainPJ.angle % 360) <= 135) {
				mainPJ.angle = 90;
			}
			else if ((mainPJ.angle % 360) > 135 && (mainPJ.angle % 360) <= 225) {
				mainPJ.angle = 180;
			}
			else if ((mainPJ.angle % 360) > 225 && (mainPJ.angle % 360) <= 315) {
				mainPJ.angle = 270;
			}
			else if ((mainPJ.angle % 360) > 315 && (mainPJ.angle % 360) <= 360) {
				mainPJ.angle = 360;
			}
		}

	}
}
