package Juego;

import Motor.Sprite;

public class UIGame extends Texto{
	
	private static UIGame interfaz = null;
	
	private UIGame() {
		super("UI", 650, 15, 800, 65, "Muertes: " + GameOver.contadorMuertes + "");
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Funcion que ejecuta la interfaz inGame
	 * @return interfaz
	 */
	public static UIGame getUI() {
		if(interfaz == null) {
			interfaz = new UIGame();
			interfaz.font = Main.w.customFont("GdashFont.otf", 32);
		}
		return interfaz;
	}
	
	
	/**
	 * Funcion que reinicia la interfaz del nivel.
	 */
	public static void ReiniciarUI() {
		interfaz = null;
	}
	


	

}
