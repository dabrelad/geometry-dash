package Juego;

import Motor.Field;
import Motor.Sprite;

public class Portal1 extends Portales {

	public static boolean changingGravity = false;

	public Portal1(int x1, int y1, int x2, int y2, String path) {
		super(x1, y1, x2, y2, path);
		terrain = true;
		solid = true;
		DEPTH = 10;
	}

	/**
	 * Invierte la gravedad del personaje.
	 */
	private static void cambiarGravedad() {
		tipoPortales portal = tipoPortales.GRAVEDAD;
		Portales.efecto(portal);
	}
	
	
	/**
	 * Comprueba si en algún momento colisiona con el portal de cambio de gravedad y la cambia. 
	 * @param mainPJ Objeto del Personaje Principal. 
	 * @param f Field. 
	 */
	public static void triggerPortal(Sprite mainPJ, Field f) {
		if ((mainPJ.collidesWithList(Mapa.portalesGravedad).size() > 0 && mainPJ.isGrounded(f) && changingGravity == false)
				|| (mainPJ.collidesWithList(Mapa.portalesGravedad).size() > 0 && PJ.saltando && changingGravity == false)
				|| (mainPJ.collidesWithList(Mapa.portalesGravedad).size() > 0 && PJ.enElTecho && changingGravity == false)) {
			changingGravity = true;
			cambiarGravedad();
		}
	}

}
