package Juego;

import Motor.Field;
import Motor.Sprite;
import Motor.Window;

public class PortalCoheteANormal extends Portales{
	
	tipoPortales portal = tipoPortales.COHETEANORMAL;
	public PortalCoheteANormal(int x1, int y1, int x2, int y2, String path) {
		super(x1, y1, x2, y2, path);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Comprueba si el personaje principal esta colisionando con algun portal de cambio de gravedad 2.
	 * @param mainPJ Objeto del Personaje Principal.
	 * @param f Field.
	 * @param w Window. 
	 */
	public static void triggerPortal(Sprite mainPJ, Field f, Window w) {
		tipoPortales portal = tipoPortales.COHETEANORMAL;
		if ((mainPJ.collidesWithList(Mapa.portalesCohete2).size() > 0)) {
			Portales.efecto(portal);
		}
	}
	
	

}
