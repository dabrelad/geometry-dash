package Juego;

import java.io.IOException;

import Motor.Field;
import Motor.Sprite;

public class Espina extends PJ {

	public Espina(int x1, int y1, int x2, int y2, String path) {
		super("espina", x1, y1, x2, y2, path);

		solid = true;
	}

	/**
	 * Esta funcion calcula constantemente si el PJ esta colisionando con alguna espina del mapa.
	 * 
	 * @param initialX1PJ posicionX1 del PJ
	 * @param initialX2PJ posicionX2 del PJ
	 * @param initialY1PJ posicionY1 del PJ
	 * @param initialY2PJ posicionY2 del PJ
	 * @param mainPJ Sprite de tu personaje princiapal
	 * @param f Field
	 * @throws IOException 
	 */
	
	public static void colisionEspina(int initialX1PJ, int initialX2PJ, int initialY1PJ, int initialY2PJ, Sprite mainPJ, Field f) throws IOException {
		if (mainPJ.collidesWithList(Mapa.espinas).size() > 0) {
			GameOver.gameOver(initialX1PJ, initialX2PJ, initialY1PJ, initialY2PJ, mainPJ);
		}
	}

}
