package Juego;

import Motor.Field;
import Motor.Sprite;
import Motor.Window;

public class TrampolinDobleSalto extends Sprite{
	/**
	 * Booleano que indica cuando cojes el trampolin
	 */
	public static boolean trampolinSalto  = false;
	private static int posicionPjTrampolinX;

	public TrampolinDobleSalto(int x1, int y1, int x2, int y2) {
		super("DobleSalto", x1, y1, x2, y2, "DobleSalto.png");
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Comprueba si el personaje principal esta colisionando con algun trampolin y ejecuta el salto. 
	 * @param mainPJ Objeto del Personaje Principal.
	 * @param w Window.
	 * @param f Field. 
	 */
	 static public void saltarPjTrampolin(Sprite mainPJ, Window w, Field f) {
		 float fuerzaSaltoTrampolin = 40.5f;
		 if (mainPJ.collidesWithList(Mapa.trampolinesDobleSalto).size() > 0 && !trampolinSalto) {
			 trampolinSalto = true;
			 posicionPjTrampolinX = mainPJ.x1;
		 }
		 else if(trampolinSalto && mainPJ.x1 >= posicionPjTrampolinX+100) {
				trampolinSalto = false;
		 }
		 if(trampolinSalto) {
			 mainPJ.y1 -= fuerzaSaltoTrampolin;
			 mainPJ.y2 -= fuerzaSaltoTrampolin;
			mainPJ.angle -= 350;
		 }
	}

}
