package Juego;

import java.awt.Font;
import java.io.IOException;
import java.util.ArrayList;

import Motor.Field;
import Motor.Window;
import Motor.Sprite;

public class UI {

	/**
	 * Booleano que indica si estas en el menu principal
	 */
	static boolean menuPrincipal = true;
	/**
	 * Booleano que indica si estas en el selector de niveles. 
	 */
	static boolean selectorNiveles = false;
	/**
	 * Booleano que indica si estas en el nivel 1
	 */
	static boolean nivel1 = true;
	
	static boolean nivel2 = false;
	
	/**
	 * Booleano que indica si estas inGame
	 */
	static boolean inGame = false;
	
	static boolean creandoNivel = false;
	/**
	 * Indica el nivel actual en el que estas: 0 = menu, 1 = nivel1...
	 */
	static int nivelActual = 0; 
	
	private static ArrayList<Sprite> MenuNiveles = new ArrayList<Sprite>();

	/**
	 * Crea el menu principal
	 * @param f Field.
	 * @param w Window.
	 * @param mainPJ Objeto del Personaje Principal. 
	 * @throws InterruptedException
	 */
	public static void menu(Field f, Window w, PJ mainPJ) throws InterruptedException {

		while (menuPrincipal) {
			Main.f.background = "backgrounds/Violet_game_bg_03_001-hd.png";
			Sprite title = new Sprite("", 177, 100, 677, 270, "Title.png");
			Sprite play = new Sprite("", 380, 340, 470, 435, "Play.png"); // proporcion entre x e y = 60
			Sprite config = new Sprite("", 530, 360, 620, 455, "Ajustes.png");
			Sprite personajes = new Sprite("", 230, 360, 320, 455, "Personajes.png");
			ArrayList<Sprite> MenuA = new ArrayList<Sprite>();
			MenuA.add(title);
			MenuA.add(play);
			MenuA.add(config); // NO VA ARREGLAR SPRITE CON GIMP
			MenuA.add(personajes);
			Main.f.draw(MenuA);
			if (f.getCurrentMouseX() > 380 && f.getCurrentMouseY() > 340 && f.getCurrentMouseX() < 470
					&& f.getCurrentMouseY() < 435) {
				Main.f.clear();
				selectorNiveles = true;
				AccederMenuNiveles(mainPJ);
			}
			Thread.sleep(20);

		}
	}

	/**
	 * Crea el selector de niveles y lleva el control de las monedas obtenidas.
	 * @param f Field.
	 * @param w Window.
	 * @param mainPJ Sprite del Personaje Principal.
	 */
	public static void AccederMenuNiveles(PJ mainPJ) {
		if (selectorNiveles) {
			Main.f.background = "backgrounds/Violet_game_bg_03_001-hd.png";
			while (selectorNiveles) {
				menuPrincipal = false;
				if (nivel1) {
					Sprite lvl1Button = new Sprite("", 200, 100, 690, 240, "GreenButton.png"); 
					Sprite play = new Sprite("", 380, 440, 470, 535, "Play.png"); // proporcion entre x e y = 60
					Texto nivelPrueba = new Texto("", 250, 120, 900, 180, "Nivel de Prueba");
					nivelPrueba.font = Main.w.customFont("GdashFont.otf", 46);
					Texto pasarNivel2 = new Texto("", 250, 550, 900, 590, "Pulsa D para ir al siguiente nivel");
					nivelPrueba.font = Main.w.customFont("GdashFont.otf", 46);
					pasarNivel2.font = Main.w.customFont("GdashFont.otf", 21);

					// Aqui pintamos las monedas segun las que hayamos cogido en el nivel
					ArrayList<Sprite> monedasNivel1 = new ArrayList<Sprite>();
					if (Mapa.monedasPrimerNivel == 0) {
						Sprite moneda1Nivel1 = new Sprite("", 500, 250, 550, 300, "MonedaAzul.png");
						Sprite moneda2Nivel1 = new Sprite("", 575, 250, 625, 300, "MonedaAzul.png");
						Sprite moneda3Nivel1 = new Sprite("", 650, 250, 700, 300, "MonedaAzul.png");
						monedasNivel1.add(moneda1Nivel1);
						monedasNivel1.add(moneda2Nivel1);
						monedasNivel1.add(moneda3Nivel1);
					}
					else if (Mapa.monedasPrimerNivel == 1) {
						Sprite moneda1Nivel1 = new Sprite("", 500, 250, 550, 300, "MonedaAzul.gif");
						Sprite moneda2Nivel1 = new Sprite("", 575, 250, 625, 300, "MonedaAzul.png");
						Sprite moneda3Nivel1 = new Sprite("", 650, 250, 700, 300, "MonedaAzul.png");
						monedasNivel1.add(moneda1Nivel1);
						monedasNivel1.add(moneda2Nivel1);
						monedasNivel1.add(moneda3Nivel1);
					}
					else if (Mapa.monedasPrimerNivel == 2) {
						Sprite moneda1Nivel1 = new Sprite("", 500, 250, 550, 300, "MonedaAzul.gif");
						Sprite moneda2Nivel1 = new Sprite("", 575, 250, 625, 300, "MonedaAzul.gif");
						Sprite moneda3Nivel1 = new Sprite("", 650, 250, 700, 300, "MonedaAzul.png");
						monedasNivel1.add(moneda1Nivel1);
						monedasNivel1.add(moneda2Nivel1);
						monedasNivel1.add(moneda3Nivel1);
					}
					else if (Mapa.monedasPrimerNivel == 3) {
						Sprite moneda1Nivel1 = new Sprite("", 500, 250, 550, 300, "MonedaAzul.gif");
						Sprite moneda2Nivel1 = new Sprite("", 575, 250, 625, 300, "MonedaAzul.gif");
						Sprite moneda3Nivel1 = new Sprite("", 650, 250, 700, 300, "MonedaAzul.gif");
						monedasNivel1.add(moneda1Nivel1);
						monedasNivel1.add(moneda2Nivel1);
						monedasNivel1.add(moneda3Nivel1);
					}


					MenuNiveles.add(lvl1Button);	
					MenuNiveles.add(play);
					MenuNiveles.add(nivelPrueba);		
					MenuNiveles.add(pasarNivel2);
					MenuNiveles.addAll(monedasNivel1);
					Main.f.draw(MenuNiveles);

					if (Main.f.getCurrentMouseX() > 380 && Main.f.getCurrentMouseY() > 440 && Main.f.getCurrentMouseX() < 470
							&& Main.f.getCurrentMouseY() < 535) {
						Main.f.clear();
						selectorNiveles = false;
						inGame = true;
						Mapa.nivelMecanicas = true;
						creandoNivel = true;
						Mapa.firstTime = true;
						nivelActual = 1;
						if(PortalCohete.changingRocket) {
							PortalCohete.changingRocket = false;
							mainPJ.changeImage("PJ.png");
							mainPJ.angle = 0;
						}
					}
					if(Main.w.getCurrentKeyPressed() == 'd' || Main.w.getCurrentKeyPressed() == 'D') {
						MenuNiveles.clear();
						Main.f.clear();
						nivel2=true;
						nivel1=false;
					}

					try {
						Thread.sleep(30);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
				else if(nivel2) {
					Sprite lvl2Button = new Sprite("", 200, 100, 690, 240, "GreenButton.png"); 
					Sprite play = new Sprite("", 380, 440, 470, 535, "Play.png"); // proporcion entre x e y = 60
					Texto nivelMecanicas = new Texto("", 250, 120, 900, 180, "Nivel mecanicas");
					nivelMecanicas.font = Main.w.customFont("GdashFont.otf", 46);
					Texto pasarNivel1 = new Texto("", 250, 550, 900, 590, "Pulsa A para ir al nivel anterior");
					pasarNivel1.font = Main.w.customFont("GdashFont.otf", 21);
					
					MenuNiveles.add(lvl2Button);
					MenuNiveles.add(play);
					MenuNiveles.add(nivelMecanicas);
					MenuNiveles.add(pasarNivel1);
					Main.f.draw(MenuNiveles);
					
					if (Main.f.getCurrentMouseX() > 380 && Main.f.getCurrentMouseY() > 440 && Main.f.getCurrentMouseX() < 470
							&& Main.f.getCurrentMouseY() < 535) {
						Main.f.clear();
						selectorNiveles = false;
						inGame = true;
						Mapa.nivelMecanicas = true;
						Mapa.firstTime = true;
						creandoNivel = true;
						nivelActual = 1;
						if(PortalCohete.changingRocket) {
							PortalCohete.changingRocket = false;
							mainPJ.changeImage("PJ.png");
							mainPJ.angle = 0;
						}
					}
					
					if(Main.w.getCurrentKeyPressed() == 'a' || Main.w.getCurrentKeyPressed() == 'A') {
						MenuNiveles.clear();
						Main.f.clear();
						nivel1=true;
						nivel2=false;
					}
					
					try {
						Thread.sleep(30);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
				}

			}
		}

	}

	/**
	 * Hace que si pulsas R en el juego puedas volver al selector de niveles.
	 * @param w Window
	 * @param f Field.
	 * @param mainPJ Sprite del Personaje Principal. 
	 */
	public static void inputEscInGame(PJ mainPJ) {
		if(Main.w.getCurrentKeyPressed() == 'r' || Main.w.getCurrentKeyPressed() == 'R') {
			nivelActual = 0;
			selectorNiveles = true;
			if (Main.fuerzaGravedad < 0) {
				Main.fuerzaGravedad = -Main.fuerzaGravedad;
			}
			mainPJ.x1 = PJ.mainX;
			mainPJ.x2 = PJ.mainX+50;
			mainPJ.y1 = PJ.mainY;
			mainPJ.y2 = PJ.mainY+50;
			Main.f.clear();
			Mapa.todos.clear();
			Main.f.resetScroll();
			Main.w.stopMusic();
			Particulas.AParticulas.clear();
			inGame = false;
			AccederMenuNiveles(mainPJ);
		}
		
	}
}
