package Juego;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import Motor.Field;
import Motor.Sprite;

public class Particulas extends Sprite {
	static Random r = new Random();
	/**
	 * Velocidad base de una particula en el eje X.
	 */
	int velocidadX = 0;
	/**
	 * Velocidad base de una particula en el eje Y.
	 */
	int velocidadY = 0;
	/**
	 * Lista de particulas.
	 */
	static ArrayList<Particulas> AParticulas = new ArrayList<Particulas>();

	public Particulas(int x1, int y1, int x2, int y2, String path) {
		super("particula", x1, y1, x2, y2, path);
		// TODO Auto-generated constructor stub

	}


	/**
	 * Creacion, update y delete de particulas
	 * @param mainPJ Objeto del personaje Principal
	 */
	static void GParticulasPJ(PJ mainPJ) {
		int count = 2;
		//Creacion, update y delete de particulas siempre que el PJ este en el suelo o en el techo
		if(mainPJ.enElSuelo) {
		inicializarParticulasMainPJ(count, mainPJ);
		AParticulas = destruirParticulas(mainPJ);
		anadirFuerza(mainPJ);
		pintarParticulas();
		}
		
		//
		else if(mainPJ.saltando) {
			AParticulas = destruirParticulas(mainPJ);
			anadirFuerza(mainPJ);
			pintarParticulas();
			
		}
	}

	/**
	 * A�ade fuerza a las particulas
	 * @param mainPJ Objeto del personaje Principal
	 */
	private static void anadirFuerza(PJ mainPJ) {
		for (int i = 0; i < AParticulas.size(); i++) {
			if (Main.fuerzaGravedad > 0) {
				AParticulas.get(i).y1 -= r.nextInt(2 + 1);
				AParticulas.get(i).y2 -= r.nextInt(2 + 1);
			} else if (Main.fuerzaGravedad < 0) {
				AParticulas.get(i).y1 += r.nextInt(1 + 1);
				AParticulas.get(i).y2 += r.nextInt(1 + 1);
			}
		}

	}

	/**
	 * Inicializa las particulas
	 * @param count Contador de particulas a generar x iteracion del bucle
	 * @param mainPJ Objeto del personaje Principal
	 */
	static void inicializarParticulasMainPJ(int count, PJ mainPJ) {
		int largadaPosible = 10;
		int anchoPosible = 15;
		int velocidadP = 5;
		for (int i = 0; i < count; i++) {
			if (mainPJ.enElSuelo && !mainPJ.enElTecho) {
				Particulas newP = new Particulas((mainPJ.x1 - 10) - r.nextInt(largadaPosible + 1),
						(mainPJ.y1 + 30) + r.nextInt(anchoPosible + 1), mainPJ.x1 - r.nextInt(largadaPosible + 1),
						mainPJ.y2, "Particula.png");
				newP.velocidadX = Main.velocidad - r.nextInt(velocidadP);
				AParticulas.add(newP);
			} else if (mainPJ.enElTecho) {
				Particulas newP = new Particulas((mainPJ.x1 - 10) - r.nextInt(largadaPosible + 1),
						(mainPJ.y2 - 30) - r.nextInt(anchoPosible + 1), mainPJ.x1 - r.nextInt(largadaPosible + 1),
						(mainPJ.y1), "Particula.png");
				newP.velocidadX = Main.velocidad - r.nextInt(velocidadP);
				AParticulas.add(newP);
			}
		}
	}

	/**
	 * Anade las particulas al field
	 */
	static void pintarParticulas() {
		Main.f.add(AParticulas);
	}
	
	
	/**
	 * Destruye las particulas cuando entran al borde izquierdo de la ventana
	 * @param mainPJ Objeto del personaje Principal
	 * @return Lista con las particulas a pintar
	 */
	static ArrayList<Particulas> destruirParticulas(PJ mainPJ) {
		ArrayList<Particulas> AParticulas2 = new ArrayList<Particulas>();
		for (int i = 0; i < AParticulas.size(); i++) {
			if (!(AParticulas.get(i).x1 + mainPJ.mainX < mainPJ.x1)) {
				AParticulas2.add(AParticulas.get(i));
			}
		}
		return AParticulas2;
	}
}
