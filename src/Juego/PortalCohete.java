package Juego;

import Motor.Field;
import Motor.Sprite;
import Motor.Window;

public class PortalCohete extends Portales {

	/**
	 * Booleano que indica cuando estas en formato cohete.
	 */
	static boolean changingRocket = false;

	public PortalCohete(int x1, int y1, int x2, int y2, String path) {
		super(x1, y1, x2, y2, path);
		terrain = true;
		solid = true;
	}

	/**
	 * Comprueba si el personaje principal esta colisionando con algun portal de transformacion a cohete. 
	 * @param mainPJ Objeto del Personaje Principal. 
	 * @param f Field.
	 * @param w Window.
	 */
	public static void triggerPortal(Sprite mainPJ, Field f, Window w) {
		if ((mainPJ.collidesWithList(Mapa.portalesCohete).size() > 0)) {
			changingRocket = true;
		}
		if (changingRocket) {
			transformRocket(mainPJ, f, w);
		}
		PortalCoheteANormal.triggerPortal(mainPJ, f, w);

	}

	/**
	 * Transforma al personaje al modo cohete.
	 * @param mainPJ Objeto del Personaje Principal.
	 * @param f Field.
	 * @param w Window. 
	 */
	private static void transformRocket(Sprite mainPJ, Field f, Window w) {
		tipoPortales portal = tipoPortales.COHETE;
		Portales.efecto(portal);
	}
}
