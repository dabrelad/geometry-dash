package Juego;

import java.io.IOException;
import java.util.ArrayList;

import Motor.Sprite;
import Motor.Field;
import Motor.Window;

public class Main {

	// VARIABLES Y OBJETOS
	/**
 	* Booleano que indica si estas muerto.
 	*/
	static public boolean muerto = false;
	/**
	 * Booleando que indica si el juego se ha terminado
	 */
	static public boolean finished = false;
	/**
	 * Valor de la fuerza de gravedad.
	 */
	static public float fuerzaGravedad = 15.0f;
	static public Field f = new Field();
	static public Window w = new Window(f);
	static public PJ mainPJ;
	/**
	 * Valor de la velocidad del PJ y del scroll de pantalla.
	 */
	static int velocidad = 15;

	// FINAL VARIABLES Y OBJETOS

	public static void main(String[] args) throws InterruptedException, IOException {
		// inicializaciones
		int sleep = 30;
		
		
		if(!UI.inGame) {
		UI.menu(f, w, mainPJ);
		GameOver.cargarContadorMuertes();
		}
		Mapa.generarSegundoNivel();
		
		while (!muerto && !finished && UI.inGame) {
			//inicializaciones
			if(UI.creandoNivel) {
				if(UI.nivel2) {
					Mapa.generarNivelMecanicas();
				}
				else if(UI.nivel1) {
					Mapa.generarSegundoNivel();
				}
				mainPJ = PJ.inicializarPJ();
				UI.creandoNivel = false;
			}

			// bucle principal
			Mapa.finalNiveles(mainPJ);
			PJ.comprobarSalto(mainPJ, w);
			//PJ.debugPJ(mainPJ, velocidad, w);
			PJ.moverPJ(mainPJ, velocidad, w);
			Mapa.scrollearFondoNiveles();
			f.add(mainPJ);
			Mapa.refrescarNivel();
			Mapa.spritesCaen();
			Espina.colisionEspina(PJ.mainX, PJ.mainX + 50, PJ.mainY, PJ.mainY + 50, mainPJ, f);
			Portal1.triggerPortal(mainPJ, f); 
			Moneda.colisionMoneda(mainPJ, f);
			TrampolinDobleSalto.saltarPjTrampolin(mainPJ, w, f);
			PortalCohete.triggerPortal(mainPJ, f, w);
			UI.inputEscInGame(mainPJ);
			Particulas.GParticulasPJ(mainPJ);
			f.draw(); 
			// sleep
			
			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
