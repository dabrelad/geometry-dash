package Juego;

public class Music {
	
	/**
	 * Funcion que ejecuta la musica del nivel.
	 */
	public static void playMusic() {
		if(Mapa.segundoNivel) {
			Main.w.playMusic("stereo-madness.wav");
		}
	}

}
