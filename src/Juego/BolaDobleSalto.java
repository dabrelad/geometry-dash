package Juego;

import Motor.Field;
import Motor.Sprite;

public class BolaDobleSalto extends Sprite {

	public BolaDobleSalto(int x1, int y1, int x2, int y2) {
		super("bola", x1, y1, x2, y2, "bola.png");
		solid = true;
	}
	
	/**
	 * Funcion que comprueba si estoy colisionando con una bola de doble salto y si pulso espacio para efectuar el doble salto
	 */
	public static void dobleSalto() {
		for (Sprite s : Main.mainPJ.collidesWithList(Mapa.bolas)) {
			PJ.inicioSaltoX = Main.mainPJ.x1;
			PJ.inicioSaltoY = Main.mainPJ.y1;
			if (Main.w.getCurrentKeyPressed() == ' ') {		
				PJ.segundaFase = false;
				PJ.saltar(Main.mainPJ);
			}
		}
	}

}
