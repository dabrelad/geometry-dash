package Juego;

import Motor.Sprite;

public class Background extends Sprite{

	public Background(int x1, int y1, int x2, int y2, String path) {
		super("bg", x1, y1, x2, y2, path);
		terrain = false;
		solid = false;
	}

}
