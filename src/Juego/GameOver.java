package Juego;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import Motor.Field;
import Motor.Sprite;

public class GameOver {

	public static int contadorMuertes = 0;

	/**
	 * 
	 * Esta función comprueba constantemente si has muerto para volver a iniciarlo
	 * correctamente
	 * 
	 * @param initialX1PJ posiciónX1 del PJ
	 * @param initialX2PJ posiciónX2 del PJ
	 * @param initialY1PJ posiciónY1 del PJ
	 * @param initialY2PJ posiciónY2 del PJ
	 * @param mainPJ      Objeto de tu personaje princiapal
	 * @param f           Field
	 * @throws IOException
	 */

	private static void guardarContadorMuertes() throws IOException {
		String ruta = "muertes.txt";
		File archivo = new File(ruta);
		BufferedWriter bw;
		if (archivo.exists()) {
			bw = new BufferedWriter(new FileWriter(archivo));
			bw.write(contadorMuertes);

		} else {
			bw = new BufferedWriter(new FileWriter(archivo));
		}
		bw.close();
	}

	/**
	 * Funcion que carga el archivo de muertes si existe y si no existe crea uno. 
	 * @throws IOException
	 */
	
	public static void cargarContadorMuertes() throws IOException {
		int contadorMuertesS;
		String ruta = "muertes.txt";
		File archivo = new File(ruta);
		if (archivo.createNewFile()) {
			FileReader f = new FileReader(archivo);
			contadorMuertes = 0;
			System.out.println(contadorMuertes);
		}
		else {
			FileReader f = new FileReader(archivo);
			contadorMuertes = f.read();
			System.out.println(contadorMuertes);
		}
	}
	
	/**
	 * Funcion que reinicia el nivel al morir.
	 * @param initialX1PJ posiciónX1 del PJ
	 * @param initialX2PJ posiciónX2 del PJ
	 * @param initialY1PJ posiciónY1 del PJ
	 * @param initialY2PJ posiciónY2 del PJ
	 * @param mainPJ      Objeto de tu personaje principal
	 * @throws IOException
	 */

	public static void gameOver(int initialX1PJ, int initialX2PJ, int initialY1PJ, int initialY2PJ, Sprite mainPJ)
			throws IOException {
		if (Main.fuerzaGravedad < 0) {
			Main.fuerzaGravedad = -Main.fuerzaGravedad;
		}
		contadorMuertes++;
		PJ.saltando = false;
		PortalCohete.changingRocket = false;
		mainPJ.x1 = initialX1PJ;
		mainPJ.x2 = initialX2PJ;
		mainPJ.y1 = initialY1PJ;
		mainPJ.y2 = initialY2PJ;
		mainPJ.angle = 0;
		mainPJ.changeImage("PJ.png");
		UIGame.ReiniciarUI();
		Particulas.AParticulas.clear();
		if (Mapa.segundoNivel) {
			Mapa.generarSegundoNivel();
			Music.playMusic();
		} else if (Mapa.nivelMecanicas) {
			Mapa.generarNivelMecanicas();
		}
		guardarContadorMuertes();
		Main.f.resetScroll();
	}
}
